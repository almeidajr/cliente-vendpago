## CLIENTE VENDPAGO

### Introdução

O cliente para gerenciamento do terminal de pagamento foi desenvolvido com o intuito de ser simples de integrado em diferentes plataformas. Por essa razão, é desacoplado da aplicação principal, sendo desenvolvido em python. Utiliza recursos de notificação do sistema de arquivos (inotify) para verificar dentro de um determinado diretório se arquivos no formato JSON foram criados ou modificados.

Este cliente foi refeito a partir da experiência adquirida no software desenvolvido por Cleiton Bueno. O refatoramento tem relação a simplificar o controle e torná-lo mais claro para futuras atualizações.


### Instalação

1.  Criar diretório /opt/broker/vendpago/ e copiar nele os arquivos vp_client.py e daemon-vendpago.sh
2.  Executar chmod +x daemon-vendpago.sh
3.  Copiar systemd/vendpago.service para /etc/systemd/system
4.  Executar systemctl daemon-reload
5.  Executar systemctl enable vendpago
6.  Executar systemctl start vendpago
7.  Para verificar se o sistema está funcionando, use: systemctl status vendpago ou journalctl -u vendpago -f


### Funcionamento

O cliente é composto por uma representação de uma **máquina de estados finita** (FSM), que trata diversos estados nos quais o terminal pode operar. São eles:

* Inicialização do terminal: *init state*
* Ociosidade: *idle state*
* Verificação: *verifying-terminal state*  --> **_Indisponível durante testes_**
* Ordem de compra simples: *single-order state*
* Ordem de compra multi: *multi-order state*
* Obtenção do protocolo: *protocol state*
* Execução da ordem de compra: *exec-order state*
* Liberação/Cancelamento da ordem: *release state*

As transições de estado são definidas pela identificação de uma ordem de compra (via inotify) e do tratamento do processo de compra através da variável de transição de estado **__STATE**

A aplicação principal escreverá no diretório padrão (/var/broker/vendpago/action) dois tipos de requisições em um arquivo (não importa o nome dele desde que seja um JSON).

#### 1. Envio de ordem de pagamento
Essas notificações poderão ser de duas formas:
* Compra simples

```JSON
{
    "notify": "order",
    "products": [
        {
            "id": 15,
            "price": 0.1
        }
    ]
}
```

* Multi-compras

```JSON
{
    "notify": "order",
    "products": [
        {
            "id": 12,
                "price": 0.2	    
        },
        {
            "id": 87,
            "price": 0.05
        }
    ]
}
```

* Confirmação/Cancelamento da compra
```JSON
{
    "notify": "release",
    "action: "confirm"
}
```

#### 2. Notificação de status do terminal e transação e recebimento de protocolo

Durante a execução da transação, a execução de alguns passos geram modificações em arquivos de estatus. A mudança desses arquivos geram notificações (também por inotify) para a aplicação principal, que trata como se fosse um *callback* o estado do terminal, da transação e, também, o código de protocolo gerado.


#### Instruções de execução

A FSM é implementada utilizando um laço de repetição ilimitada e para que as transições ocorram é necessário que a execução do laço seja interrompida em períodos tratados pelo timer da biblioteca que gerencia o processamento concorrente (30 ms - tempo ajustado empiricamente). Essa biblioteca, gevent, utilizada na primeira implementação feita por Cleiton Bueno foi mantida por essa razão. Ela é simples, mas tem como _drawback_ funcionar apenas nas versões 2.7 do python. Um meio de contornar o problema é definir no _virtualenv_ a versão mais recente do python 2.7 para executar esse cliente.

##### Configuração
A porta usb deve ser configurada na variável **_device**

#### Bibliotecas python necessárias
* os
* subprocess
* ~~sys~~ (será removida)
* time
* json
* serial
* gevent
* ~~pprint~~ (será removida)
* watchdog
* PyCRC

#### Validação
A validação dessa versão foi realizada em uma raspberryPi e uma toradex iMX6, ambas com processador ARM 32/64 bits.

Durante esse processo, notou-se que dependendo da carga do sistema, o cliente de pagamento não garantia os timings na resposta/leitura da serial. Uma maneira de resolver esse problema foi definir uma maior prioridade desse processo no escalonador do sistema. Isso causa maior consumo da CPU pelo cliente de pagamento, mas garante que ele funcione adequadamente.


## Contato

Em caso de dúvidas, sugestões e problemas, contate:
* Carlos Almeida Jr - carlos@aroeira.io
