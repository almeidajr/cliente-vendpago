#! /usr/bin/python -u
import re

def find_serial():
    settingsFile="/opt/imach/conf/settings.conf"
    fileHandler = open(settingsFile, 'r')
    fileLines = fileHandler.readlines()

    paymentBlock = False
    serialPort=""
    for line in fileLines:
        if "[PAYMENT]" in line:
            paymentBlock = True

        if paymentBlock:
            if "SERIAL" in line:
                pattern = re.search(r'=(.*)', line, re.DOTALL)
                serialPort = pattern.group(1)
                break

    return serialPort.strip()

serial=find_serial()

if serial is not None:
    print("[INFO] Payment serial found in: {}".format(serial))
else:
    print("[WARNING] Payment serial not found");
